module Types
  ( KnowledgeBase
  , Logic
  ) where

-- |Defines a knowledge base class where known information can be stored.
class KnowledgeBase a where
  -- |If possible, adds information to the knowledge base.
  -- On failure, will return contradicting information.
  add :: (Logic l, Monad m) => a -> l -> m (Maybe [l])
  -- |If possible, removes information from the knowledge base.
  remove :: (Logic l, Monad m) => a -> l -> m Bool
  -- |Pulls all known information from a knowledge base.
  pull :: (Logic l, Monad m) => a -> m [l]

-- |Defines a logical language.
class Logic a where
  -- |Checks if a statement is true or false (if that can be determined).
  resolve :: (KnowledgeBase b, Monad m) => b -> a -> m (Maybe Bool)
  -- |Uses a logical statement to query the knowledge base, returns all
  -- possible answers.
  query :: (KnowledgeBase b, Monad m) => b -> a -> m [a]
