{-# LANGUAGE GADTs #-}
module Modal
  () where

import Control.Monad.Operational
import Control.Monad((<=<))

import Data.Graph.Inductive.Graph

import Types
import Worlds

data Modal a where
  AllWorlds :: Logic a => a -> Modal a
  AWorld :: Logic a => a -> Modal a

type ModalM m a = ProgramT Modal m a

runModal :: (Logic a, KnowledgeBase b, DynGraph g, Monad m) =>
  Worlds g c d b -> ModalM m a -> m a
runModal ws = eval ws <=< viewT

eval :: (Logic a, KnowledgeBase b, DynGraph g, Monad m) =>
  Worlds g c d b -> ProgramViewT Modal m a -> m a
eval _ (Return a) = return a
