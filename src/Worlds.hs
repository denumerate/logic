{-# LANGUAGE RankNTypes #-}
module Worlds
  ( Worlds
  , emptyWorlds
  ) where

import Data.Graph.Inductive.Graph

import Data.Map(Map)
import qualified Data.Map as M

import Types

-- |A graph of worlds and accompanying knowledge bases.
data Worlds g a b k = Worlds { graph :: DynGraph g => g a b
                             , bases :: KnowledgeBase b => Map a k
                             }

-- |Creates an empty set of worlds.
emptyWorlds :: (DynGraph g,KnowledgeBase k) => Worlds g a b k
emptyWorlds = Worlds { graph = empty
                     , bases = M.empty
                     }
